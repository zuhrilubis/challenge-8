import React from "react";
import "./button.css";

const Button = (props) => {
  let buttonTitle = props.title ?? "Klik Me!";
  let buttonOnClick = props.onClick ?? null;
  let buttonClassName = props.className ?? "buttonComponent";
  let buttonDisabled = props.disabled ?? null;

  return (
    <div className={buttonClassName} onClick={buttonOnClick} disabled={buttonDisabled} >
      {buttonTitle}
    </div>
  );
};

export default Button;
