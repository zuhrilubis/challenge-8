import React from "react";
import "./input.css";

const Input = (props) => {
  const typeInput = props.type ?? "text";
  const placeholderInput = props.placeholder ?? "fill me please!";
  const styleInput = props.style ?? "";
  const nameInput = props.name ?? "";
  const valueInput = props.value ?? "";
  const onChangeInput = props.onChange ?? "";

  return (
    <div className="inputComponent">
      <input
        type={typeInput}
        placeholder={placeholderInput}
        style={{ styleInput }}
        name={nameInput}
        value={valueInput}
        onChange={(event) => {
          onChangeInput(event);
        }}
      />
    </div>
  );
};

export default Input;
