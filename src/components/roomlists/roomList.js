import React, { useState } from "react";
import "./roomList.css";
import { Link } from "react-router-dom";

export default function RoomList() {
  const [posts, setPosts] = useState([
    {
      roomId: "1",
      roomName: "Room One",
      playerOneId: "Username 1",
      playerOneChoice: "gunting",
      playerOneResult: "lose",
      playerTwoId: "Username 2",
      playerTwoChoice: "batu",
      playerTwoResult: "win",
      roomStatus: "Complete",
      winner: "Username 2",
    },
    {
      roomId: "2",
      roomName: "Room Two",
      playerOneId: "Username 1",
      playerOneChoice: "batu",
      playerOneResult: "draw",
      playerTwoId: "Username 2",
      playerTwoChoice: "batu",
      playerTwoResult: "draw",
      roomStatus: "Complete",
      winner: "Nil (draw)",
    },
    {
      roomId: "3",
      roomName: "Room Three",
      playerOneId: "Username 1",
      playerOneChoice: "kertas",
      playerOneResult: "win",
      playerTwoId: "Username 2",
      playerTwoChoice: "batu",
      playerTwoResult: "lose",
      roomStatus: "Complete",
      winner: "Username 1",
    },
    {
      roomId: "4",
      roomName: "Room Four",
      playerOneId: "Username 1",
      playerOneChoice: "kertas",
      playerOneResult: "waiting player 2.....",
      playerTwoId: "waiting player 2.....",
      playerTwoChoice: "",
      playerTwoResult: "",
      roomStatus: "available",
      winner: "..........",
    },
    {
      roomId: "5",
      roomName: "Room Five",
      playerOneId: "Username 1",
      playerOneChoice: "kertas",
      playerOneResult: "waiting player 2.....",
      playerTwoId: "waiting player 2.....",
      playerTwoChoice: "",
      playerTwoResult: "",
      roomStatus: "available",
      winner: "..........",
    },
    {
      roomId: "6",
      roomName: "Room Six",
      playerOneId: "Username 1",
      playerOneChoice: "kertas",
      playerOneResult: "waiting player 2.....",
      playerTwoId: "waiting player 2.....",
      playerTwoChoice: "",
      playerTwoResult: "",
      roomStatus: "available",
      winner: "..........",
    },
    {
      roomId: "7",
      roomName: "Room Seven",
      playerOneId: "Username 1",
      playerOneChoice: "kertas",
      playerOneResult: "lose",
      playerTwoId: "Username 2",
      playerTwoChoice: "gunting",
      playerTwoResult: "win",
      roomStatus: "complete",
      winner: "Username 2",
    },
  ]);
  return (
    <div className="containerRoomList">
      {posts.map((post) => {
        return (
          <Link to={`/dashboard/${post.roomStatus}/${post.roomId}`} style={{textDecoration: "none"}}>
          <div className="singleRoomList">
            <div className="singleListRoomName">{post.roomName}</div>
            <div>Winner: {post.winner} </div>
            <div>Status: {post.roomStatus} </div>
          </div>
          </Link>
        );
      })}
    </div>
  );
}
