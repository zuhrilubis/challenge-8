import React from "react";
import "./navBar.css";
import LogoTulisan from "../../Elements/logo-tulisan/logoTulisan";
import { Link } from "react-router-dom";

const HomeNavBar = () => {
  return (
    <div className="navbarHome">
      <Link to="/home" style={{textDecoration:"none"}}>
        <LogoTulisan />
      </Link>
      <div className="homeMiddleBar">
        <Link to="/dashboard" style={{ textDecoration: "none" }}>
          <div className="homeDashboardBar">DASHBOARD</div>
        </Link>
        <div className="workBar">WORK</div>
        <div className="contactBar">CONTACT</div>
        <div className="aboutMeBar">ABOUT ME</div>
      </div>
      <Link to="/registration" style={{ textDecoration: "none" }}>
        <div className="homeSignupBar">SIGN UP</div>
      </Link>
      <Link to="/login" style={{ textDecoration: "none" }}>
        <div className="homeLoginBar">LOGIN</div>
      </Link>
    </div>
  );
};

export default HomeNavBar;
