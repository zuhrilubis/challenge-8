import React, { useState } from "react";
import "./navBar.css";
import { Link } from "react-router-dom";
import { useFormik } from "formik";
import { loginSchema } from "../../Elements/validationSchema";

const GameNavBar = () => {
  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema: loginSchema,
  });
  
  const [user, setUser] = useState({ username: formik.values.username, password: formik.values.password });
  
  const handleLogout = () => {
    setUser({ ...user, username: "", password: "" });
    const accessTokenLog = "";
    localStorage.setItem("accessTokenLog", accessTokenLog);
  };
  
  return (
    <div className="navbarGame">
      <Link to="/home" style={{ textDecoration: "none" , marginLeft:"-2em"}}>
        <div className="gameHomeBar">HOME</div>
      </Link>
      <Link to="/dashboard" style={{ textDecoration: "none" , marginLeft:"2em", marginRight:"62.5em"}}>
        <div className="gameDashboardBar">DASHBOARD</div>
      </Link>
      <Link to="/home" style={{ textDecoration: "none"}}>
      <div className="gameLogoutBar" onClick={handleLogout}>LOGOUT</div>
      </Link>
    </div>
  );
};

export default GameNavBar;
