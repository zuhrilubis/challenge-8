import React, { useEffect, useState } from "react";
import "./playerVsCom.css";
import GameNavBar from "../../components/NavBars/gameNavBar";
import PlayerTitle from "../../components/playerTitle/playerTitle";

const PlayerVsComPage = () => {
  const accessToken = localStorage.getItem("accessTokenLog");
  const [playerChoice, setplayerChoice] = useState("");
  const [comChoice, setcomChoice] = useState("");
  const [player, setplayer] = useState(accessToken);
  const [middleColor, setmiddleColor] = useState("");
  const [border, setborder] = useState("");
  const [result, setresult] = useState("");
  const [fontWeight, setfontWeight] = useState("");

  const pilihanRandomCom = () => {
    const pilihan = ["rock", "paper", "scissors"];
    setcomChoice(pilihan[Math.floor(Math.random() * pilihan.length * 1)]);
  };

  useEffect(() => {
    if (playerChoice === "rock") {
      switch (comChoice) {
        case "paper":
          return setresult("COM WIN");
        case "scissors":
          return setresult("PLAYER WIN");
        case "rock":
          return setresult("DRAW");
      }
    } else if (playerChoice === "paper") {
      switch (comChoice) {
        case "rock":
          return setresult("PLAYER WIN");
        case "scissors":
          return setresult("COM WIN");
        case "paper":
          return setresult("DRAW");
      }
    } else if (playerChoice === "scissors") {
      switch (comChoice) {
        case "rock":
          return setresult("COM WIN");
        case "paper":
          return setresult("PLAYER WIN");
        case "scissors":
          return setresult("DRAW");
      }
    }
  });

  return (
    <div className="playerVsComContainer">
      <div className="gameNavBar">
        <GameNavBar />
      </div>
      <PlayerTitle leftTitle={player} rightTitle="COM" />
      <div className="player1Vsplayer2-content-matchVsCom">
        <div className="player1Vsplayer2-content-left">
          <div className="player1Choices">
            <div>
              <img
                className="player1Vsplayer2-rock-leftVsCom"
                src={require("../../Elements/images/batu.png")}
                alt=""
                style={{
                  backgroundColor: playerChoice === "rock" ? "lightpink" : "",
                }}
                onClick={() => {
                  if (playerChoice === "") {
                    setplayerChoice("rock");
                    setcomChoice(pilihanRandomCom);
                    setfontWeight("bold");
                  }
                  if (middleColor === "") {
                    setmiddleColor("green");
                  }
                  if (border === "") {
                    setborder("none");
                  }
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-leftVsCom"
                src={require("../../Elements/images/kertas.png")}
                alt=""
                style={{
                  backgroundColor: playerChoice === "paper" ? "lightpink" : "",
                }}
                onClick={() => {
                  if (playerChoice === "") {
                    setplayerChoice("paper");
                    setcomChoice(pilihanRandomCom);
                    setfontWeight("bold");
                  }
                  if (middleColor === "") {
                    setmiddleColor("green");
                  }
                  if (border === "") {
                    setborder("none");
                  }
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-scissors-leftVsCom"
                src={require("../../Elements/images/gunting.png")}
                alt=""
                style={{
                  backgroundColor:
                    playerChoice === "scissors" ? "lightpink" : "",
                }}
                onClick={() => {
                  if (playerChoice === "") {
                    setplayerChoice("scissors");
                    setcomChoice(pilihanRandomCom);
                    setfontWeight("bold");
                  }
                  if (middleColor === "") {
                    setmiddleColor("green");
                  }
                  if (border === "") {
                    setborder("none");
                  }
                }}
              />
            </div>
          </div>
        </div>
        <div
          className="player1Vsplayer2-content-middleVsCom"
          style={{
            backgroundColor: middleColor,
            border: border,
            fontWeight: fontWeight,
          }}
        >
          {result === "" ? "VS" : `${result}`}
        </div>
        <div className="player1Vsplayer2-content-rightVsCom">
          <div className="comChoices">
            <div>
              <img
                className="player1Vsplayer2-rock-rightVsCom"
                src={require("../../Elements/images/batu.png")}
                alt=""
                style={{
                  backgroundColor: comChoice === "rock" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightVsCom"
                src={require("../../Elements/images/kertas.png")}
                alt=""
                style={{
                  backgroundColor: comChoice === "paper" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-scissors-rightVsCom"
                src={require("../../Elements/images/gunting.png")}
                alt=""
                style={{
                  backgroundColor: comChoice === "scissors" ? "lightpink" : "",
                }}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="vsComBottomText">
        {result === "COM WIN" &&  (
          <div className="vsComBottomTextResult">
            DON'T GIVE UP, <br></br> TRY AGAIN..GOOD LUCK..!
          </div>
        )}
        {result === "PLAYER WIN" && (
          <div className="vsComBottomTextResult">
            CONGRATULATION..!! YOU WIN..
          </div>
        )}
        {result === "DRAW" && (
          <div className="vsComBottomTextResult">
            ALMOST.., <br></br> TRY AGAIN..GOOD LUCK..!
          </div>
        )}
        {result === "" && (
          <div className="vsComBottomTextReady">
            ARE YOU READY TO FIGHT..?!!
          </div>
        )}
      </div>
    </div>
  );
};

export default PlayerVsComPage;
