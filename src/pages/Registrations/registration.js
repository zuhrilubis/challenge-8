import React, { useState } from "react";
import "./registration.css";
import "../Homes/home.css";
import HomeNavBar from "../../components/NavBars/homeNavBar";
import { Link, useNavigate } from "react-router-dom";
import LogoTulisan from "../../Elements/logo-tulisan/logoTulisan";
import { useFormik } from "formik";
import { registrationSchema } from "../../Elements/validationSchema";

const RegistrationPage = () => {
  const navigate = useNavigate();
  const [user, setUser] = useState({
    username: "",
    email: "",
    password: "",
    confirmpassword: "",
  });

  const handleRegister = () => {
    setUser({
      ...user,
      username: formik.values.username,
      email: formik.values.email,
      password: formik.values.password,
    });
    const accessTokenReg = formik.values.username;
    localStorage.setItem("accessTokenReg", accessTokenReg);
    navigate("/login");
  };

  const formik = useFormik({
    initialValues: {
      username: "",
      email: "",
      password: "",
      confirmpassword: "",
    },
    validationSchema: registrationSchema,
    onSubmit: handleRegister,
  });

  console.log(formik);

  return (
    <div className="pageRegistration">
      <div className="homeNavBar">
        <HomeNavBar />
      </div>
      <div className="registration-midle-container">
        <div className="coverRegistration">
          <div className="registrationLogo">
            <Link to="/home" style={{ textDecoration: "none" }}>
              <LogoTulisan />
            </Link>
          </div>
          <div className="subTitleRegistration">
            Create your account to get more pleasure, enjoy it!
          </div>
          <form onSubmit={formik.handleSubmit}>
            <input
              className="inputComponent"
              type="text"
              placeholder="Username"
              name="username"
              {...formik.getFieldProps("username")}
            />
            <div className="errorInputRegistration">
              {formik.touched.username && formik.errors.username && (
                <div className="registrationError">
                  {formik.errors.username}{" "}
                </div>
              )}
            </div>

            <input
              className="inputComponent"
              type="email"
              placeholder="Email"
              name="email"
              {...formik.getFieldProps("email")}
            />
            <div className="errorInputRegistration">
              {formik.touched.email && formik.errors.email && (
                <div className="registrationError">{formik.errors.email} </div>
              )}
            </div>

            <input
              className="inputComponent"
              type="password"
              placeholder="Password"
              name="password"
              {...formik.getFieldProps("password")}
            />
            <div className="errorInputRegistration">
              {formik.touched.password && formik.errors.password && (
                <div className="registrationError">
                  {formik.errors.password}{" "}
                </div>
              )}
            </div>
            <input
              className="inputComponent"
              type="password"
              placeholder="Confirm Password"
              name="confirmpassword"
              {...formik.getFieldProps("confirmpassword")}
            />
            <div className="errorInputRegistration">
              {formik.touched.confirmpassword &&
                formik.errors.confirmpassword && (
                  <div className="registrationError">
                    {formik.errors.confirmpassword}{" "}
                  </div>
                )}
            </div>

            <div className="buttonRegistration">
              <button
                className="buttonComponent"
                type="submit"
              >
                SIGN UP
              </button>
            </div>
          </form>
          <div className="textBelow-registration">
            Already have an account?{" "}
            <span>
              <Link to="/login" style={{ textDecoration: "none" }}>
                Sign In here
              </Link>
            </span>{" "}
            please!
          </div>
        </div>
      </div>
    </div>
  );
};

export default RegistrationPage;
