import React, { useEffect, useState } from "react";
import "./createRoom.css";
import GameNavBar from "../../components/NavBars/gameNavBar";
import Input from "../../components/inputs/input";
import Button from "../../components/buttons/button";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import { createRoomSchema } from "../../Elements/validationSchema";

const CreateRoomPage = () => {
  const navigate = useNavigate();
  const [isErrors, setIsErrors] = useState(true);
  const [roomName, setroomName] = useState("");
  const [pilihan, setpilihan] = useState("");

  const formik = useFormik({
    initialValues: {
      roomName: "",
    },
    onSubmit: (values) => {
      console.log("form value", values);
    },
    validationSchema: createRoomSchema,
  });

  console.log(formik);

  const handleSave = () => {
    setroomName({
      ...roomName,
      roomName: formik.values.roomName,
    });

    navigate("/createdroom");
  };


  useEffect(() => {
    Object.keys(formik.errors).length === 0
      ? setIsErrors(false)
      : setIsErrors(true);
  }, [formik.errors]);

console.log(pilihan)

  return (
    <div className="createRoomContainer">
      <div className="gameNavBar">
        <GameNavBar />
      </div>
      <div className="creatRoomContent">
        <div className="labelCreateRoom">Create Your Room Here!</div>
        <div className="createRoomInput">
          <input
            className="inputComponent"
            type="text"
            placeholder="Input Your Game Room Name"
            name="roomName"
            value={formik.values.roomName}
            onChange={formik.handleChange}
          />
          <div className="errorInputRoomName">
            {formik.errors.roomName ?? (
              <div className="roomNameError">{formik.errors.roomName} </div>
            )}
          </div>
        </div>
        <div className="yourChoice">Your Choice Please!</div>
        <div className="choices">
          <div>
            <img
              className="rock"
              src={require("../../Elements/images/batu.png")}
              alt=""
              style={{
                backgroundColor: pilihan === "rock" ? "lightpink" : "",
              }}
              onClick={() => {
                if (pilihan === "") {
                  setpilihan("rock");
                }
              }}
              name="scissors"
              value={formik.values.pilihan}
              onChange={formik.handleChange}
            />
          </div>
          <div>
            <img
              className="paper"
              src={require("../../Elements/images/kertas.png")}
              alt=""
              style={{
                backgroundColor: pilihan === "paper" ? "lightpink" : "",
              }}
              onClick={() => {
                if (pilihan === "") {
                  setpilihan("paper");
                }
              }}
              name="scissors"
              value={formik.values.pilihan}
              onChange={formik.handleChange}
            />
          </div>
          <div>
            <img
              className="scissors"
              src={require("../../Elements/images/gunting.png")}
              alt=""
              style={{
                backgroundColor: pilihan === "scissors" ? "lightpink" : "",
              }}
              onClick={() => {
                if (pilihan === "") {
                  setpilihan("scissors");
                }
              }}
              name="scissors"
              value={formik.values.pilihan}
              onChange={formik.handleChange}
            />
          </div>
        </div>
        <Link to="/createroom/createdroom" style={{ textDecoration: "none" }}>
          {" "}
          <button
            className="buttonComponent"
            onClick={handleSave}
            disabled={isErrors === true ? true : false}
          >
            SAVE
          </button>
        </Link>
      </div>
    </div>
  );
};

export default CreateRoomPage;
