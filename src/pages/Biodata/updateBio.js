import React, { useState } from "react";
import "./updateBio.css";
import "../Homes/home.css";
import HomeNavBar from "../../components/NavBars/homeNavBar";
import { Link, useNavigate } from "react-router-dom";
import { useFormik, yupToFormErrors } from "formik";
import { updateBioSchema } from "../../Elements/validationSchema";
import LogoTulisan from "../../Elements/logo-tulisan/logoTulisan";

const UpdateBioPage = () => {
  const navigate = useNavigate();
  const [user, setUser] = useState({
    fullname: "",
    phonenumber: "",
    address: "",
  });

  const handleUpdateBio = () => {
    setUser({
      ...user,
      fullname: formik.values.fullname,
      phonenumber: formik.values.phonenumber,
      address: formik.values.address,
    });

    navigate("/dashboard");
  };

  const formik = useFormik({
    initialValues: {
      fullname: "",
      phonenumber: "",
      address: "",
    },
    validationSchema: updateBioSchema,
    onSubmit: handleUpdateBio,
  });

  console.log(formik);

  return (
    <div className="pageUpdateBio">
      <div className="homeNavBar">
        <HomeNavBar />
      </div>
      <div className="updateBio-midle-container">
        <div className="coverUpdateBio">
          <div className="updateBioLogo">
            <Link to="/home" style={{ textDecoration: "none" }}>
              <LogoTulisan />
            </Link>
          </div>
          <div className="subTitleUpdateBio">
            Update your biodata, enjoy it!
          </div>
          <form onSubmit={formik.handleSubmit}>
            <input
              className="inputComponent"
              type="text"
              placeholder="Full Name"
              name="fullname"
              {...formik.getFieldProps("fullname")}
            />
            <div className="errorInputUpdateBio">
              {formik.touched.fullname && formik.errors.fullname && (
                <div className="updateBioError">
                  {formik.errors.fullname}{" "}
                </div>
              )}
            </div>

            <input
              className="inputComponent"
              type="number"
              placeholder="Phone Number"
              name="phonenumber"
              {...formik.getFieldProps("phonenumber")}
            />
            <div className="errorInputUpdateBio">
              {formik.touched.phonenumber && formik.errors.phonenumber && (
                <div className="updateBioError">
                  {formik.errors.phonenumber}{" "}
                </div>
              )}
            </div>

            <input
              className="inputComponent"
              type="text"
              placeholder="Address"
              name="address"
              {...formik.getFieldProps("address")}
            />
            <div className="errorInputUpdateBio">
              {formik.touched.address && formik.errors.address && (
                <div className="updateBioError">
                  {formik.errors.address}{" "}
                </div>
              )}
            </div>

            <div className="buttonUpdateBio">
              <button className="buttonComponent" type="submit">
                UPDATE
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default UpdateBioPage;
