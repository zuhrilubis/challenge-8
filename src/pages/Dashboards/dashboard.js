import React from "react";
import "./dashboard.css";
import RoomList from "../../components/roomlists/roomList";
import GameNavBar from "../../components/NavBars/gameNavBar";
import { Link } from "react-router-dom";

const DashboardPage = () => {
  const userLoggedIn = localStorage.getItem("accessTokenLog");
  
  return (
    <div className="dashboardContainer">
      <div className="gameNavBar">
        <GameNavBar />
      </div>
      <div className="dash-containerBelow">
        <div className="leftBelow">
          <div className="playVSCom">
            <Link to="/playerVScom" style={{ textDecoration: "none" }}>
              <div className="playVSComBox">PLAY VS COM</div>
            </Link>
          </div>
          <div className="singleLine">
            <hr></hr>
          </div>
          <div className="createNewRoom">
            <Link to="/createroom" style={{ textDecoration: "none" }}>
              Create New Room
            </Link>
          </div>
          <div className="roomList">
            {/* tambahkan data player 2, tambahkan aplikasi single post yang request usePAram (post.js)*/}
            <RoomList />
          </div>
        </div>
        <div className="rightBelow">
          <div className="userPictPart">
            <div className="userPictBox">
              <img
                className="userPicture"
                src={require("../../Elements/logo-tulisan/logoTulisan")}
                alt="user's picture"
              />
            </div>
            <div className="profilName">{userLoggedIn}</div>
          </div>
          <div className="userBio">
            <div>Fullname : .... </div>
            <div>Address : .... </div>
            <div>Phone Number: ....</div>
          </div>
          <div className="updateBio">
            <Link
              to="/updatebio"
              style={{ textDecoration: "none", fontWeight: "bold" }}
            >
              Update your Biodata here
            </Link>
          </div>
          <Link to="/gameHistory" style={{ textDecoration: "none" }}>
            <div className="gameHistory"> Game History </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default DashboardPage;
