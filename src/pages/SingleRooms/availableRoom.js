import React, { useState } from "react";
import "./availableRoom.css";
import GameNavBar from "../../components/NavBars/gameNavBar";
import TitleRoomName from "../../components/roomName/roomName";
import PlayerTitle from "../../components/playerTitle/playerTitle";

const AvailableRoomPage = () => {
  const [pilihan1, setpilihan1] = useState("");
  const [pilihan2, setpilihan2] = useState("");
  const [middle, setmiddle] = useState("");
  const [border, setborder] = useState("");
  const [roomname, setroomname] = useState("Room One");
  const [playerone, setplayerone] = useState("PLAYER 1");
  const [playertwo, setplayertwo] = useState("PLAYER 2");
  const [middleText, setmiddleText] = useState("VS");

  return (
    <div className="availableRoom-Container">
      <div className="gameNavBar">
        <GameNavBar />
      </div>
      <TitleRoomName title={roomname} />
      <PlayerTitle leftTitle={playerone} rightTitle={playertwo} />
      <div className="player1Vsplayer2-content-matchAvailable">
        <div className="player1Vsplayer2-content-left">
          <div className="player1Choices">
            <div>
              <img
                className="player1Vsplayer2-rock-leftAvailable"
                src={require("../../Elements/images/batu.png")}
                alt=""
                style={{
                  backgroundColor: pilihan1 === "rock" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-leftAvailable"
                src={require("../../Elements/images/kertas.png")}
                alt=""
                style={{
                  backgroundColor: pilihan1 === "paper" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-scissors-leftAvailable"
                src={require("../../Elements/images/gunting.png")}
                alt=""
                style={{
                  backgroundColor: pilihan1 === "scissors" ? "lightpink" : "",
                }}
              />
            </div>
          </div>
        </div>
        <div
          className="player1Vsplayer2-content-middleAvailable"
          style={{
            backgroundColor: middle,
            border: border,
          }}
        >
          {middleText}
        </div>
        <div className="player1Vsplayer2-content-right">
          <div className="comChoices">
            <div>
              <img
                className="player1Vsplayer2-rock-rightAvailable"
                src={require("../../Elements/images/batu.png")}
                alt=""
                style={{
                  backgroundColor: pilihan2 === "rock" ? "lightpink" : "",
                }}
                onClick={() => {
                  if (pilihan2 === "") {
                    setpilihan2("rock");
                    setpilihan1("scissors");
                    setmiddleText("PLAYER 2 WIN");
                  }
                  if (middle === "") {
                    setmiddle("green");
                  }
                  if (border === "") {
                    setborder("none");
                  }
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightAvailable"
                src={require("../../Elements/images/kertas.png")}
                alt=""
                style={{
                  backgroundColor: pilihan2 === "paper" ? "lightpink" : "",
                }}
                onClick={() => {
                  if (pilihan2 === "") {
                    setpilihan2("paper");
                    setpilihan1("paper");
                    setmiddleText("DRAW");
                  }
                  if (middle === "") {
                    setmiddle("green");
                  }
                  if (border === "") {
                    setborder("none");
                  }
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightAvailable"
                src={require("../../Elements/images/gunting.png")}
                alt=""
                style={{
                  backgroundColor: pilihan2 === "scissors" ? "lightpink" : "",
                }}
                onClick={() => {
                  if (pilihan2 === "") {
                    setpilihan2("scissors");
                    setpilihan1("rock");
                    setmiddleText("PLAYER 1 WIN");
                  }
                  if (middle === "") {
                    setmiddle("green");
                  }
                  if (border === "") {
                    setborder("none");
                  }
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AvailableRoomPage;
