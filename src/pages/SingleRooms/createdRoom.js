import React, { useState } from "react";
import "./createdRoom.css";
import GameNavBar from "../../components/NavBars/gameNavBar";
import TitleRoomName from "../../components/roomName/roomName";
import PlayerTitle from "../../components/playerTitle/playerTitle";

const CreatedRoomPage = () => {
  const [pilihan, setpilihan] = useState("rock");
  const [middle, setmiddle] = useState("green");
  const [border, setborder] = useState("none");
  const [roomname, setroomname] = useState("Room One");
  const [playerone, setplayerone] = useState("PLAYER 1");

  return (
    <div className="createdRoom-Container">
      <div className="gameNavBar">
        <GameNavBar />
      </div>
      <TitleRoomName title={roomname} />
      <PlayerTitle leftTitle={playerone} rightTitle="Waiting Player 2 ..... " />
      <div className="player1Vsplayer2-content-matchCreated">
        <div className="player1Vsplayer2-content-left">
          <div className="player1Choices">
            <div>
              <img
                className="player1Vsplayer2-rock-leftCreated"
                src={require("../../Elements/images/batu.png")}
                alt=""
                style={{
                  backgroundColor: pilihan === "rock" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-leftCreated"
                src={require("../../Elements/images/kertas.png")}
                alt=""
                style={{
                  backgroundColor: pilihan === "paper" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-scissors-leftCreated"
                src={require("../../Elements/images/gunting.png")}
                alt=""
                style={{
                  backgroundColor: pilihan === "scissors" ? "lightpink" : "",
                }}
              />
            </div>
          </div>
        </div>
        <div
          className="player1Vsplayer2-content-middleCreated"
          style={{
            backgroundColor: middle,
            border: border,
          }}
        >
          VS
        </div>
        <div className="player1Vsplayer2-content-rightCreated">
          <div className="comChoices">
            <div>
              <img
                className="player1Vsplayer2-rock-rightCreated"
                src={require("../../Elements/images/batu.png")}
                alt=""
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightCreated"
                src={require("../../Elements/images/kertas.png")}
                alt=""
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-scissors-rightCreated"
                src={require("../../Elements/images/gunting.png")}
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CreatedRoomPage;
