import React, { useState } from "react";
import "./completeRoom.css";
import GameNavBar from "../../components/NavBars/gameNavBar";
import { useParams } from "react-router-dom";
import TitleRoomName from "../../components/roomName/roomName";
import PlayerTitle from "../../components/playerTitle/playerTitle";

const CompleteRoomPage = () => {
  const [pilihan1, setpilihan1] = useState("paper");
  const [pilihan2, setpilihan2] = useState("scissors");
  const [middle, setmiddle] = useState("green");
  const [border, setborder] = useState("none");
  const [roomname, setroomname] = useState("Room One");
  const [middleText, setmiddleText] = useState("PLAYER 2 WIN");

  const { roomId } = useParams;

  return (
    <div className="completeRoom-Container">
      <div className="gameNavBar">
        <GameNavBar />
      </div>
      <TitleRoomName title={roomname} />
      <PlayerTitle leftTitle="PLAYER 1" rightTitle="PLAYER 2" />
      <div className="player1Vsplayer2-content-matchComplete">
        <div className="player1Vsplayer2-content-left">
          <div className="player1Choices">
            <div>
              <img
                className="player1Vsplayer2-rock-leftComplete"
                src={require("../../Elements/images/batu.png")}
                alt=""
                style={{
                  backgroundColor: pilihan1 === "rock" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-leftComplete"
                src={require("../../Elements/images/kertas.png")}
                alt=""
                style={{
                  backgroundColor: pilihan1 === "paper" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-scissors-leftComplete"
                src={require("../../Elements/images/gunting.png")}
                alt=""
                style={{
                  backgroundColor: pilihan1 === "scissors" ? "lightpink" : "",
                }}
              />
            </div>
          </div>
        </div>
        <div
          className="player1Vsplayer2-content-middleComplete"
          style={{
            backgroundColor: middle,
            border: border,
          }}
        >
          {middleText}
        </div>
        <div className="player1Vsplayer2-content-right">
          <div className="comChoices">
            <div>
              <img
                className="player1Vsplayer2-rock-rightComplete"
                src={require("../../Elements/images/batu.png")}
                alt=""
                style={{
                  backgroundColor: pilihan2 === "rock" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightComplete"
                src={require("../../Elements/images/kertas.png")}
                alt=""
                style={{
                  backgroundColor: pilihan2 === "paper" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightComplete"
                src={require("../../Elements/images/gunting.png")}
                alt=""
                style={{
                  backgroundColor: pilihan2 === "scissors" ? "lightpink" : "",
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CompleteRoomPage;
