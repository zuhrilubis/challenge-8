import React from "react";
import "./home.css";
import HomeNavBar from "../../components/NavBars/homeNavBar";
import Button from "../../components/buttons/button";
import { Link } from "react-router-dom";

const HomePage = () => {
  return (
    // Halaman-1
    <div className="homeContainer">
      <div className="homeNavBar">
        <HomeNavBar />
      </div>
      <div className="homeTitle">PLAY TRADITIONAL GAME</div>
      <div className="homeSubtitle">Experience New Traditional Game</div>
      <div className="playNowButton">
        <Link to="/dashboard" style={{textDecoration:"none"}}>
          <Button title="PLAY NOW" />
        </Link>
      </div>
      <div className="theStory">THE STORY</div>
      <div className="homeIconDown">
        <img
          className="iconDown"
          src={require("../../Elements/images/down-arrow.png")}
          alt=""
        />
      </div>
    </div>
  );
};

export default HomePage;
