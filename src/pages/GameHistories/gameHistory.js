import React from "react";
import "./gameHistory.css";
import GameNavBar from "../../components/NavBars/gameNavBar";
import GameHistoryUser from "../../components/gameHistoryUsers/gameHistoryUser";

const GameHistyoryPage = () => {
  return (
    <div className="gameHistory-Container">
      <div className="gameNavBar">
        <GameNavBar />
      </div>
      <div className="gameHistory-container-below">
        <div className="gameHistory-content">
          <div className="gameHistory-title">Game History</div>
          <div className="tabel-gameHistory">
            <GameHistoryUser />
          </div>
        </div>
      </div>
    </div>
  );
};

export default GameHistyoryPage;
