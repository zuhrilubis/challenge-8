import React, { useState } from "react";
import "./login.css";
import HomeNavBar from "../../components/NavBars/homeNavBar";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import { loginSchema } from "../../Elements/validationSchema";
import LogoTulisan from "../../Elements/logo-tulisan/logoTulisan";

const LoginPage = () => {
  const navigate = useNavigate();

  const [user, setUser] = useState({ username: "", password: "" });

  const handleLogin = () => {
    setUser({
      ...user,
      username: formik.values.username,
      password: formik.values.password,
    });
    const accessTokenLog = formik.values.username;
    localStorage.setItem("accessTokenLog", accessTokenLog);
    navigate("/dashboard");
  };

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema: loginSchema,
    onSubmit: handleLogin,
  });

  console.log(formik);

  return (
    <div className="pageLogin">
      <div className="homeNavBar">
        <HomeNavBar />
      </div>
      <div className="login-midle-container">
        <div className="coverLogin">
          <div className="loginLogo">
            <Link to="/home" style={{ textDecoration: "none" }}>
              <LogoTulisan />
            </Link>
          </div>
          <div className="subTitleLogin">
            Welcome to the game! <br></br>Before playing the game, Sign in first
            and then, enjoy the game!
          </div>
          <form onSubmit={formik.handleSubmit}>
            <input
              className="inputComponent"
              type="text"
              placeholder="Username"
              name="username"
              {...formik.getFieldProps("username")}
            />
            <div className="errorInputLogin">
              {formik.touched.username && formik.errors.username && (
                <div className="registrationError">
                  {formik.errors.username}{" "}
                </div>
              )}
            </div>

            <input
              className="inputComponent"
              type="password"
              placeholder="Password"
              name="password"
              {...formik.getFieldProps("password")}
            />
            <div className="errorInputLogin">
              {formik.touched.password && formik.errors.password && (
                <div className="registrationError">
                  {formik.errors.password}{" "}
                </div>
              )}
            </div>

            <div className="buttonLogin">
              <button
                className="buttonComponent"
                type="submit"
              >
                SIGN IN
              </button>
            </div>
          </form>
          <div className="textBelow-login">
            Don't have account yet?{" "}
            <span>
              <Link to="/registration" style={{ textDecoration: "none" }}>
                Sign Up here
              </Link>
            </span>{" "}
            please!
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
