import * as yup from "yup";

const accessToken = localStorage.getItem("accessTokenLog");
const accessLenght = accessToken.length;
const loginSchema = yup.object({
  username: yup
    .string()
    .required("username should not empty")
    .min(4, "username too short, min 4 character"),
  password: yup
    .string()
    .required("password should not empty")
    .matches(/[a-z]/g, "should contain at least 1 lowercase")
    .matches(/[A-Z]/g, "should contain at least 1 uppercase")
    .matches(/[0-9]/g, "should contain at least 1 number")
    .matches(/^\S*$/, "should not contain space")
    .min(8, "password too short, min 8 character"),
});

const registrationSchema = yup.object({
  username: yup
    .string()
    .required("username should not empty")
    .min(4, "username too short, min 4 character"),
  email: yup
    .string()
    .email("Invalid email format")
    .required("email should not empty"),
  password: yup
    .string()
    .matches(/[a-z]/g, "should contain at least 1 lowercase")
    .matches(/[A-Z]/g, "should contain at least 1 uppercase")
    .matches(/[0-9]/g, "should contain at least 1 number")
    .matches(/^\S*$/, "should not contain space")
    .min(8, "password too short, min 8 character")
    .required("password should not empty"),
  confirmpassword: yup
    .string()
    .matches(/[a-z]/g, "should contain at least 1 lowercase")
    .matches(/[A-Z]/g, "should contain at least 1 uppercase")
    .matches(/[0-9]/g, "should contain at least 1 number")
    .matches(/^\S*$/, "should not contain space")
    .min(8, "password too short, min 8 character")
    .required("confirm password should not empty")
    .oneOf([yup.ref("password")], "Password must match"),
});

const updateBioSchema = yup.object({
  fullname: yup
    .string()
    .required("fullname should not empty")
    .min(accessLenght + 1, "fullname should be longer then username"),
  phonenumber: yup
    .string()
    .required("phone number should not empty")
    .min(10, "phone number too short, min 10 character"),
  address: yup.string().required("address should not empty"),
});

const createRoomSchema = yup.object({
  roomName: yup
    .string()
    .required("room name should not empty")
    .min(4, "room name too short, min 4 character"),
});

export { loginSchema, registrationSchema, createRoomSchema, updateBioSchema };
