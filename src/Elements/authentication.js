import { Navigate } from "react-router-dom";

const Authentication = ({ children }) => {
  const accessTokenLog = localStorage.getItem("accessTokenLog");
  const accessTokenReg = localStorage.getItem("accessTokenReg");

  if (accessTokenReg === null) {
    return <Navigate to={"/registration"} />;
  }
  if (accessTokenReg && !accessTokenLog) {
    return <Navigate to={"/login"} />;
  }
  if (accessTokenLog !== accessTokenReg) {
    return <Navigate to={"/registration"} />;
  }
  if (accessTokenLog === accessTokenReg) {
    return <>{children}</>;
  }
};

export default Authentication;
