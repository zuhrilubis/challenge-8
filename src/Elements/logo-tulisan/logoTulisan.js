import React from "react";
import "./logoTulisan.css";

const LogoTulisan = () => {
  return (
    <div className="logo-tulisan">
      <div className="kotakHome">
        <div className="lingkaranHome">
          <img
            src={require("../../Elements/images/gunting.png")}
            className="modelisiScissors"
            alt=""
          />
          <img
            src={require("../../Elements/images/quest.png")}
            className="modelisiQuest"
            alt=""
          />
          <img
            src={require("../../Elements/images/kertas.png")}
            className="modelisiPaper"
            alt=""
          />
          <img
            src={require("../../Elements/images/batu.png")}
            className="modelisiRock"
            alt=""
          />
        </div>
      </div>
    </div>
  );
};

export default LogoTulisan;
